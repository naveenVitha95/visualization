# Group: group 4
    
# Question

RQ: Is there a correlation between GDP per capita and happiness score?

Null hypothesis: There is no correlation between GDP per capita and happiness score. 

Alternative hypothesis: There is a correlation between GDP per capita and happiness score. 

# Dataset

URL: https://www.kaggle.com/yamaerenay/world-happiness-report-preprocessed

## Column Headings

```
> dataset <- read_csv("2020_report.csv")
> colnames(dataset)
 [1] "country"           "happiness_score"   "gdp_per_capita"    "social_support"    "health"           
 [6] "freedom"           "generosity"        "government_trust"  "dystopia_residual" "continent"

```